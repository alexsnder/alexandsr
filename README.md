<img align="right" style="float:right; width: 33%;" src="https://github-readme-stats.vercel.app/api/top-langs/?username=alexandsr&show_icons=true&theme=github_dark&layout=compact">


### My name is Alexander, sometimes Lev online.
I do web and Java development. Learning C++ & Go.

---

<div id="languages">
  <img src="html.png" style="width: 5%" > 
  <img src="css.png" style="width: 5%"> 
  <img src="js.png" style="width: 5%">
  <img src="line.png" style="width: 2%">
  <img src="https://github.com/devicons/devicon/blob/master/icons/java/java-original.svg" style="width: 5%">
  <img src="line.png" style="width: 2%">
  <img src="https://github.com/devicons/devicon/blob/master/icons/python/python-original.svg" style="width: 5%">

<!--   
<img src="line.png" style="width: 2%"> 
  <img src="https://em-content.zobj.net/thumbs/120/google/110/flag-for-sweden_1f1f8-1f1ea.png" style="width: 5%"> 
  <img src="https://em-content.zobj.net/thumbs/120/google/110/flag-for-england_1f3f4-e0067-e0062-e0065-e006e-e0067-e007f.png" style="width: 5%"> 
  <img src="https://em-content.zobj.net/thumbs/120/google/110/flag-for-germany_1f1e9-1f1ea.png" style="width: 5%"> 
-->
</div>
<div>
  <img src="https://github.com/devicons/devicon/blob/master/icons/vuejs/vuejs-original.svg" style="width: 5%">
  <img src="https://github.com/devicons/devicon/blob/master/icons/tailwindcss/tailwindcss-plain.svg" style="width: 5%">
  <img src="https://github.com/devicons/devicon/blob/master/icons/sass/sass-original.svg" style="width: 5%">
  <img src="line.png" style="width: 2%">
  <img src="https://github.com/alexandsr/alexandsr/assets/61996958/7af88d5b-343e-4572-8260-eddb6bd8ad57" style="width: 5%">
  <img src="line.png" style="width: 2%">
  <img src="mojo.png" style="width: 5%">
</div>

<!--   

<div id="tools">
  <img src="leaflet.png" style="width: 4%"> 
  <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/jquery/jquery-plain.svg" style="width: 4%">
  <img src="line.png" style="width: 2%"> 
  <img src="https://upload.wikimedia.org/wikipedia/commons/8/84/Matplotlib_icon.svg" style="width: 4%">
</div>
-->
